" Call PlugInstall to install

call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdtree'

Plug 'airblade/vim-gitgutter'

Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-surround'

Plug 'w0rp/ale'

Plug 'othree/xml.vim'
Plug 'plasticboy/vim-markdown'
Plug 'godlygeek/tabular'
Plug 'ekalinin/Dockerfile.vim'

call plug#end()
