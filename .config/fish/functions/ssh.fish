function ssh
    if string match -q 'tmux*' (ps -p (string trim (ps -p %self -o ppid=)) -o comm=)
        tmux rename-window "ssh ☇ $argv"
        command ssh $argv
        tmux set-window-option automatic-rename "on" 1>/dev/null
    else
        command ssh $argv
    end
end
